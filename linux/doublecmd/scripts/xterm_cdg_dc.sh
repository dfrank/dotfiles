#!/bin/bash

# usage:
#  bash xterm_fzf.sh -d     : opens xterm with fzf in DIRS mode
#  bash xterm_fzf.sh -f     : opens xterm with fzf in FILES mode

tmp_file="/tmp/last_fzf_catch"

catch=$(bash /home/dimon/dotfiles/linux/fzf/xterm_cdg.sh)
cd_to_dir="$catch"

if [[ "$cd_to_dir" != "" ]]
then
   doublecmd --client $cd_to_dir
fi


