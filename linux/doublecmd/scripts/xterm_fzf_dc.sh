#!/bin/bash

# usage:
#  bash xterm_fzf.sh -d     : opens xterm with fzf in DIRS mode
#  bash xterm_fzf.sh -f     : opens xterm with fzf in FILES mode

tmp_file="/tmp/last_fzf_catch"

xterm_fzf_mode="-f"

while getopts df opt; do
  case $opt in
  d)
      xterm_fzf_mode="-d"
      ;;
  f)
      xterm_fzf_mode="-f"
      ;;
  esac
done

catch=$(bash /home/dimon/dotfiles/linux/fzf/xterm_fzf.sh $xterm_fzf_mode)
cd_to_dir="$catch"

if [[ "$cd_to_dir" != "" ]]
then
   doublecmd --client $(pwd)/$cd_to_dir
fi


