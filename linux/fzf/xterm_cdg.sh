#!/bin/bash

if [[ "$1" != "" ]]
then
   tmp_file="$1"
else
   tmp_file="/tmp/last_fzf_catch"
fi

catch=""
echo --$tmp_file
xterm -e "cdscuts_glob_echo | /usr/bin/ruby /home/dimon/.fzf/fzf -x > $tmp_file" && catch="$(cat $tmp_file)" && rm $tmp_file
echo $catch

