#!/bin/bash

# usage:
#  bash xterm_fzf.sh -d     : opens xterm with fzf in DIRS mode
#  bash xterm_fzf.sh -f     : opens xterm with fzf in FILES mode

tmp_file="/tmp/last_fzf_catch"

mode="mode_files"

while getopts df opt; do
  case $opt in
  d)
      mode="mode_dirs"
      ;;
  f)
      mode="mode_files"
      ;;
  esac
done

catch=""

if [[ "$mode" == "mode_dirs" ]]
then
   xterm -e "find * -path '*/\.*' -prune -o -type d -print 2> /dev/null | /usr/bin/ruby /home/dimon/.fzf/fzf -x > $tmp_file" && catch="$(cat $tmp_file)" && rm $tmp_file
elif [[ "$mode" == "mode_files" ]]
then
   xterm -e "/usr/bin/ruby /home/dimon/.fzf/fzf -x > $tmp_file" && catch="$(cat $tmp_file)" && rm $tmp_file
fi

echo $catch

