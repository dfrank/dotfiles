#!/bin/bash

NOW=`date +%Y.%m.%d.%H.%M.%S`

BACKUP_PATH="/home/dimon/backups"
BACKUP_FILENAME="dfrank-systembackup-$NOW.tgz"

BACKUP_TMP_PATH="/tmp"
BACKUP_TMP_FULL_FILENAME="$BACKUP_TMP_PATH/$BACKUP_FILENAME"
BACKUP_FULL_FILENAME="$BACKUP_PATH/$BACKUP_FILENAME"

BOOL_OK=1

echo "--------------------------------------"
echo "["`date +%Y.%m.%d\ %H:%M:%S`"]" "creating $BACKUP_TMP_FULL_FILENAME ..."
tar cpzf $BACKUP_TMP_FULL_FILENAME  \
  --exclude=run                     \
  --exclude=proc                    \
  --exclude=lost+found              \
  --exclude=sys                     \
  --exclude=home                    \
  --exclude=tmp                     \
  --exclude=var/log                 \
  --exclude=var/cache               \
  --exclude=media                   \
  --exclude=mnt                     \
  --exclude=dev                     \
  --exclude=var/spool               \
  --exclude=opt                     \
  --exclude=var/lib/docker          \
  -C / .

if [ $? != 0 ]; then
   BOOL_OK=0
fi

echo "["`date +%Y.%m.%d\ %H:%M:%S`"]" \
  "moving $BACKUP_TMP_FULL_FILENAME to $BACKUP_FULL_FILENAME ..."

mkdir -p $BACKUP_PATH
mv $BACKUP_TMP_FULL_FILENAME $BACKUP_FULL_FILENAME
if [ $? != 0 ]; then
   BOOL_OK=0
fi

if [ $BOOL_OK == 1 ]; then
   echo "["`date +%Y.%m.%d\ %H:%M:%S`"]" \
     "System backup successfully created: $BACKUP_FULL_FILENAME"
   exit 0
else
   echo "["`date +%Y.%m.%d\ %H:%M:%S`"]" \
     "ERROR: there were errors during creating system backup: $BACKUP_FULL_FILENAME"
   exit 1
fi

